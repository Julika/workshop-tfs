/*
* Создайте такой тип DeclarativeNumber, чтобы для него работала следующая конструкция
*
* const myDeclarativeNumber = new DeclarativeNumber(5);
*
* const result = myDeclarativeNumber.add(5).minus(2).multiply(3).devide(2) + 2
* result; // 14
* */

class DeclarativeNumber {}

module.exports = DeclarativeNumber;
